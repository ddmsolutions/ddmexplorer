(function(angular) {
    'use strict';
    angular.module('ddmApps').provider('fileManagerConfig', function() {
        
        var serviceUrl = 'http://sap.ddmsolutions.it/apps(bD1pdCZjPTgwMA==)/ddmexplorer/service.json';

        var values = {
            appName: 'angular-filemanager v1.5',
            defaultLang: 'en',
            multiLang: true,
            
            //Services
            //DDM Explorer
            listUrl: serviceUrl,
            uploadUrl: serviceUrl,
            renameUrl: serviceUrl,
            copyUrl: 'http://192.168.3.202:8080/ddmconnector/php-local/index.php',
            moveUrl: 'http://192.168.3.202:8080/ddmconnector/php-local/index.php',
            removeUrl: serviceUrl,
            editUrl: 'http://192.168.3.202:8080/ddmconnector/php-local/index.php',
            getContentUrl: 'http://192.168.3.202:8080/ddmconnector/php-local/index.php',
            getDetailsUrl: 'http://192.168.3.202:8080/ddmconnector/php-local/index.php',
            createFolderUrl: serviceUrl,
            downloadFileUrl: serviceUrl,
            downloadMultipleUrl: 'http://192.168.3.202:8080/ddmconnector/php-local/index.php',
            compressUrl: 'http://192.168.3.202:8080/ddmconnector/php-local/index.php',
            extractUrl: 'http://192.168.3.202:8080/ddmconnector/php-local/index.php',
            permissionsUrl: 'http://192.168.3.202:8080/ddmconnector/php-local/index.php',
            manageFavouritesUrl: serviceUrl,
            loginUrl : 'http://sap.ddmsolutions.it/apps(bD1pdCZjPTgwMA==)/ddmdocupro/service.json',
            serverUrl: 'http://sap.ddmsolutions.it',
            getDashboardUrl: serviceUrl,
            
            //Forms
            formsFolder: '/forms/',
            
            companyName: 'DDM Solutions',
            logo: 'src/img/logo.png',
            
            //Apps
            explorerId: 'explorer',
            dashboardId: 'dashboard',
            
            /*listUrl: 'http://localhost/ddmconnector/php/handler.php',
            uploadUrl: 'http://localhost/ddmconnector/php/handler.php',
            renameUrl: 'http://localhost/ddmconnector/php/handler.php',
            copyUrl: 'http://localhost/ddmconnector/php/handler.php',
            moveUrl: 'http://localhost/ddmconnector/php/handler.php',
            removeUrl: 'http://localhost/ddmconnector/php/handler.php',
            editUrl: 'http://localhost/ddmconnector/php/handler.php',
            getContentUrl: 'http://localhost/ddmconnector/php/handler.php',
            createFolderUrl: 'http://localhost/ddmconnector/php/handler.php',
            downloadFileUrl: 'http://localhost/ddmconnector/php/handler.php',
            downloadMultipleUrl: 'http://localhost/ddmconnector/php/handler.php',
            compressUrl: 'http://localhost/ddmconnector/php/handler.php',
            extractUrl: 'http://localhost/ddmconnector/php/handler.php',
            permissionsUrl: 'http://localhost/ddmconnector/php/handler.php',*/
            
            basePath: '/',

            searchForm: true,
            sidebar: true,
            breadcrumb: true,
            allowedActions: {
                upload: true,
                rename: true,
                move: true,
                copy: true,
                edit: true,
                changePermissions: false,
                compress: true,
                compressChooseName: true,
                extract: true,
                download: true,
                downloadMultiple: true,
                preview: true,
                remove: true,
                createFolder: true,
                pickFiles: false,
                pickFolders: false,
                details: true,
                versioning: true,
                manageFavourites: true,
                manageTags: true
            },

            multipleDownloadFileName: 'ddm_downloadMultiplo.zip',
            filterFileExtensions: [],
            showExtensionIcons: true,
            showSizeForDirectories: false,
            useBinarySizePrefixes: false,
            downloadFilesByAjax: false,
            previewImagesInModal: true,
            enablePermissionsRecursive: true,
            compressAsync: false,
            extractAsync: false,
            pickCallback: null,

            isEditableFilePattern: /\.(txt|diff?|patch|svg|asc|cnf|cfg|conf|html?|.html|cfm|cgi|aspx?|ini|pl|py|md|css|cs|js|jsp|log|htaccess|htpasswd|gitignore|gitattributes|env|json|atom|eml|rss|markdown|sql|xml|xslt?|sh|rb|as|bat|cmd|cob|for|ftn|frm|frx|inc|lisp|scm|coffee|php[3-6]?|java|c|cbl|go|h|scala|vb|tmpl|lock|go|yml|yaml|tsv|lst)$/i,
            

            isPdfFilePattern: /\.(pdf|PDF|Pdf|pDf|pdF|PDf|pDF|PdF)$/i,
            isImageFilePattern: /\.(jpe?g|gif|bmp|png|svg|tiff?)$/i,
            isExtractableFilePattern: /\.(gz|tar|rar|g?zip|zip|ZIP)$/i,
            isDocFilePattern: /\.(doc|docx|docm|txt)$/i,
            isExcelFilePattern: /\.(xlxs|xls|xlsm|xlsx)$/i,
            isCsvFilePattern: /\.(csv)$/i,
            isEmailFilePattern: /\.(msg|eml)$/i,
            isXmlFilePattern: /\.(xml)$/i,
            
            tplPath: 'src/templates',
            //Custom: hidePermission
            hidePermissions: true,
            hideFavourite: false,
            hideExtra: false
        };

        return {
            $get: function() {
                return values;
            },
            set: function (constants) {
                angular.extend(values, constants);
            }
        };

    });
})(angular);
