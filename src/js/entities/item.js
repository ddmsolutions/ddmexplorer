(function(angular) {
    'use strict';
    angular.module('ddmApps').factory('item', ['fileManagerConfig', 'chmod', function(fileManagerConfig, Chmod) {

        var Item = function(model, path, currentDockey=null) {
            /*var rawModel = {
                name: model && model.name || '',
                path: path || [],
                type: model && model.type || 'file',
                size: model && parseInt(model.size || 0),
                date: parseMySQLDate(model && model.date),
                perms: new Chmod(model && model.rights),
                
                /*Custom: Status,Version,Details
                status: model && model.status || '',
                version: model && model.version || '',
                details: model && model.details || '',
              
                
                content: model && model.content || '',
                recursive: false,
                fullPath: function() {
                    var path = this.path.filter(Boolean);
                    return ('/' + path.join('/') + '/' + this.name).replace(/\/\//, '/');
                }
            };*/

            var rawModel = {
                dockey: model && model.DOCKEY || currentDockey || '',
                currentDockey: currentDockey || [],
                dokar: model && model.DOKAR || 'FAP',
                name: model && model.NAME || '',
                path: path || [],
                type: model && model.TYPE || 'file',
                //size: model && parseInt(model.size || 0),
                size: model && model.SIZE,
                //date: parseMySQLDate(model && model.date),
                date: model && model.DATE || '',
                perms: new Chmod(model && model.RIGHTS),
                status: model && model.STATUS || '',
                version: model && model.VERSION || '',
                
                labels: model && model.LABELS,
                notes: model && model.NOTES,
                
                details: model && model.details || '',
                content: model && model.content || '',
                
                icon: model && this.getIcon(model.NAME) || '',
                
               // favourite: false, 
                favourite: model && this.convertFavourite(model.FAVOURITE) || false,
                recursive: false,
                fullPath: function() {
                    var path = this.path.filter(Boolean);
                    return ('/' + path.join('/') + '/' + this.name).replace(/\/\//, '/');
                },
                
                fullDockeyPath: function () {
                    var dockey = this.currentDockey;
                    return ('/' + dockey.join('/') + '/' + this.dockey).replace(/\/\//, '/');
                }
                
                //fullPath: ''
            };
           

            console.log("Model");
            console.log(rawModel);
            console.log("-----------"); 

            this.error = '';
            this.processing = false;

            this.model = angular.copy(rawModel);
            this.tempModel = angular.copy(rawModel);

            function parseMySQLDate(mysqlDate) {
                var d = (mysqlDate || '').toString().split(/[- :]/);
                return new Date(d[0], d[1] - 1, d[2], d[3], d[4], d[5]);
            }
        };
        
        Item.prototype.getIcon = function(filename) {
            if (fileManagerConfig.isImageFilePattern.test(filename)){
                return 'far fa-image';
            }
            if (fileManagerConfig.isPdfFilePattern.test(filename)){
                return 'far fa-file-pdf';
            }
            
            if (fileManagerConfig.isExtractableFilePattern.test(filename)){
                return 'far fa-file-archive';
            }
            
            if (fileManagerConfig.isDocFilePattern.test(filename)){
                return 'far fa-file-word';
            }
            
            if (fileManagerConfig.isExcelFilePattern.test(filename) ||fileManagerConfig.isCsvFilePattern.test(filename) ){
                return 'far fa-file-excel';
            }
            
            if (fileManagerConfig.isEmailFilePattern.test(filename)){
                return 'far fa-envelope';
            }
            
            if (fileManagerConfig.isXmlFilePattern.test(filename)){
                return 'fa fa-code';
            }
        };

        Item.prototype.update = function() {
            angular.extend(this.model, angular.copy(this.tempModel));
        };

        Item.prototype.revert = function() {
            angular.extend(this.tempModel, angular.copy(this.model));
            this.error = '';
        };

        Item.prototype.isFolder = function() {
            return this.model.type === 'dir';
        };

        Item.prototype.isFile = function() {
            return this.model.type === 'file';
        };
        
        Item.prototype.isEditable = function() {
            return !this.isFolder() && fileManagerConfig.isEditableFilePattern.test(this.model.NAME);
        };
        
        //Is Image
        Item.prototype.isImage = function() {
            return fileManagerConfig.isImageFilePattern.test(this.model.NAME);
        };
         
        //Custom: Is pdf
        Item.prototype.isPdf = function() {
            return fileManagerConfig.isPdfFilePattern.test(this.model.NAME);
        };

        Item.prototype.isCompressible = function() {
            return this.isFolder();
        };

        Item.prototype.isExtractable = function() {
            return !this.isFolder() && fileManagerConfig.isExtractableFilePattern.test(this.model.NAME);
        };

        Item.prototype.isSelectable = function() {
            return (this.isFolder() && fileManagerConfig.allowedActions.pickFolders) || (!this.isFolder() && fileManagerConfig.allowedActions.pickFiles);
        };
        
        Item.prototype.isFavourite = function(){
            return this.model.favourite;
        };
        
        Item.prototype.convertFavourite = function (isFavourite){
            if (isFavourite === "true"){
                return true;
            } else if (isFavourite === "false"){
                return false;
            }              
        };

        return Item;
    }]);
})(angular);