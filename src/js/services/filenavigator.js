(function(angular) {
    'use strict';
    angular.module('ddmApps').service('fileNavigator', [
        'apiMiddleware', 'fileManagerConfig', 'item', function (ApiMiddleware, fileManagerConfig, Item) {

        var FileNavigator = function() {
            this.apiMiddleware = new ApiMiddleware();
            this.requesting = false;
            this.fileList = [];
            this.currentPath = this.getBasePath();
            /*this.currentPath = {
                path: this.getBasePath(),
                dockey : this.getCurrentDockey()
            };*/
            this.currentDockeyPath = [''];
            this.currentDockey = '';
            this.currentDokar = 'FAP';
            this.history = [];
            this.error = '';
            this.listType = '';
            this.refreshSidebar = true;
            
            this.currentLabels = '';
            
            this.isFavourite = false;
            this.isRecent = false;
            
            
            
            this.onRefresh = function() {};
        };
        
       /* FileNavigator.prototype.getCurrentDockeyPath = function(){
          return this.currentDockey;  
        };*/
        
        FileNavigator.prototype.getBasePath = function() {
            var path = (fileManagerConfig.basePath || '').replace(/^\//, '');
            return path.trim() ? path.split('/') : [];
        };

        FileNavigator.prototype.deferredHandler = function(data, deferred, code, defaultMsg) {
            if (!data || typeof data !== 'object') {
                this.error = 'Error %s - Bridge response error, please check the API docs or this ajax response.'.replace('%s', code);
            }
            if (code == 404) {
                this.error = 'Error 404 - Backend bridge is not working, please check the ajax response.';
            }
            if (code == 200) {
                this.error = null;
            }
            if (!this.error && data.RETURN_CODE !== '000') {
                this.error = data.MESSAGES.MESSAGE;
            }
            /*if (!this.error && data.error) {
                this.error = data.error.message;
            }
            if (!this.error && defaultMsg) {
                this.error = defaultMsg;
            }*/
            if (this.error) {
                return deferred.reject(data);
            }
            return deferred.resolve(data);
        };

        FileNavigator.prototype.list = function() {
            return this.apiMiddleware.list(this.currentPath, this.currentDockey, this.deferredHandler.bind(this), this.listType);
        };
        
        //Refresh navigator
        FileNavigator.prototype.refresh = function(listType=null) {

            if (listType === 'R' || listType === 'F'){
                this.listType = listType;
                this.currentDockey = '';
                this.currentPath = '';
                this.refreshSidebar = false;
                
                if (listType === 'R'){
                    this.isRecent = true;
                    this.isFavourite = false;
                } else if (listType === 'F'){
                    this.isFavourite = true;
                    this.isRecent = false;
                }
                
            } else if(listType === 'E') {
                this.listType = '';
                this.currentDockey = '';
                this.currentPath = '';
                this.refreshSidebar = true;
            }
            
            var self = this;
            if (! self.currentPath.length) {
                self.currentPath = this.getBasePath();
            }
            
            if (!self.currentDockeyPath.length){
                self.currentDockeyPath = [''];
            }
            
            var path = self.currentPath.join('/');
            
            //var dockey = self.currentDockey.join('/');
            
            self.requesting = true;
            self.fileList = [];
            return self.list(listType).then(function(data) {
                //@NOTE definizione attuale modello
                self.fileList = (data.ITEMS || []).map(function(file) {
                    return new Item(file, self.currentPath, self.currentDockeyPath);
                });
                
                if (self.refreshSidebar){
                    self.buildTree(path);
                }
                self.onRefresh();
                
                 
            }).finally(function() {
                self.requesting = false;
               
            });
        };
        
        FileNavigator.prototype.buildTree = function(path) {
            var flatNodes = [], selectedNode = {};

            function recursive(parent, item, path) {
                var absName = path ? (path + '/' + item.model.name) : item.model.name;
                if (parent.name && parent.name.trim() && path.trim().indexOf(parent.name) !== 0) {
                    parent.nodes = [];
                }
                if (parent.name !== path) {
                    parent.nodes.forEach(function(nd) {
                        recursive(nd, item, path);
                    });
                } else {
                    for (var e in parent.nodes) {
                        if (parent.nodes[e].name === absName) {
                            return;
                        }
                    }
                    parent.nodes.push({item: item, name: absName, nodes: []});
                }
                
                parent.nodes = parent.nodes.sort(function(a, b) {
                    return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : a.name.toLowerCase() === b.name.toLowerCase() ? 0 : 1;
                });
            }

            function flatten(node, array) {
                array.push(node);
                for (var n in node.nodes) {
                    flatten(node.nodes[n], array);
                }
            }

            function findNode(data, path) {
                return data.filter(function (n) {
                    return n.name === path;
                })[0];
            }

            //!this.history.length && this.history.push({name: '', nodes: []});
            !this.history.length && this.history.push({ name: this.getBasePath()[0] || '', nodes: [] });

            flatten(this.history[0], flatNodes);
            selectedNode = findNode(flatNodes, path);
            selectedNode && (selectedNode.nodes = []);
            
            for (var o in this.fileList) {
                var item = this.fileList[o];
                item instanceof Item && item.isFolder() && recursive(this.history[0], item, path);
            }

            
        };

        FileNavigator.prototype.folderClick = function(item, isFavourite = false, isRecent = false) {
            this.listType = '';
            this.currentPath = [];   
            this.refreshSidebar = true;
           this.currentDokar = item.model.dokar;
           if (isFavourite === true){
               this.isFavourite = false;
           }
           if (isRecent === true){
               this.isRecent = false;
           }
            
            
          //  this.dockey = '';
            if (item && item.isFolder()) {
                this.currentPath = item.model.fullPath().split('/').splice(1);
                this.currentDockeyPath = item.model.fullDockeyPath().split('/').splice(1);
                this.currentDockey = item.model.dockey;
            //    this.dockey = item.model.dockey;
            }
           console.log("Current Dockey Path" + this.currentDockeyPath);
            
            this.refresh();
        };
        
        
        //Create labels
        FileNavigator.prototype.createLabels = function (item){
           self = this;
           console.log(item.labels);
            angular.forEach(item.labels,function(label){
                console.log("LblDesc");
                console.log(label.LBLDESC);
                self.currentLabels += label.LBLDESC + '<br>';
            });
        };
        
        //Create labels
        FileNavigator.prototype.resetLabels = function (item){
           this.currentLabels = '';
        };

        FileNavigator.prototype.upDir = function() {
            if (this.currentPath[0]) {
                this.currentPath = this.currentPath.slice(0, -1);
                this.refresh();
            }
            
            if (this.currentDockeyPath[0]) {
                this.currentDockeyPath = this.currentDockeyPath.slice(0, -1);
                this.refresh();
            }
        };

        FileNavigator.prototype.goTo = function(index) {
            this.currentPath = this.currentPath.slice(0, index+1);
            this.currentDockeyPath = this.currentDockeyPath.slice(0, index+1);
            this.currentDockey = this.currentDockeyPath[index];
            
            //console.log(this);
            
            //console.log("Current path: " + this.currentPath);
            //console.log( this.currentPath);
            
            //console.log("Current dockey: " + this.currentDockeyPath);
            //console.log(this.currentDockeyPath);
           
            
            //console.log("index:");
            //console.log(index);
            this.refresh();
        };

        FileNavigator.prototype.fileNameExists = function(fileName) {
            return this.fileList.find(function(item) {
                return fileName && item.model.name.trim() === fileName.trim();
            });
        };

        FileNavigator.prototype.listHasFolders = function() {
            return this.fileList.find(function(item) {
                return item.model.type === 'dir';
            });
        };

        FileNavigator.prototype.getCurrentFolderName = function() {
            return this.currentPath.slice(-1)[0] || '/';
        };

        return FileNavigator;
    }]);
})(angular);
