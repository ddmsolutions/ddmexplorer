(function(angular) {
    'use strict';
    angular.module('ddmApps').service('dashboardManage', [
        'apiMiddleware', 'fileManagerConfig', function (ApiMiddleware, fileManagerConfig) {

        var dashboardManage = function() {
            this.apiMiddleware = new ApiMiddleware();
            this.dashboardApps = this.getDashboardApps();
        };
        
       /* FileNavigator.prototype.getCurrentDockeyPath = function(){
          return this.currentDockey;  
        };*/
        
        dashboardManage.prototype.deferredHandler = function(data, deferred, code, defaultMsg) {
            if (!data || typeof data !== 'object') {
                this.error = 'Error %s - Bridge response error, please check the API docs or this ajax response.'.replace('%s', code);
            }
            if (code == 404) {
                this.error = 'Error 404 - Backend bridge is not working, please check the ajax response.';
            }
            if (code == 200) {
                this.error = null;
            }
            if (!this.error && data.RETURN_CODE !== '000') {
                this.error = data.MESSAGES.MESSAGE;
            }
            /*if (!this.error && data.error) {
                this.error = data.error.message;
            }
            if (!this.error && defaultMsg) {
                this.error = defaultMsg;
            }*/
            if (this.error) {
                return deferred.reject(data);
            }
            return deferred.resolve(data);
        };

        dashboardManage.prototype.dashboard = function() {
            return this.apiMiddleware.getDashboard();
        };
        
        dashboardManage.prototype.getDashboardApps = function(){
            var dashboardApps = [];
            this.dashboard().then(function(data) {
                angular.forEach(data.MODULES, function(module) {
                    dashboardApps.push({id:module.MODULEID, moduleType:module.MODULETYPE, title:module.TITLE, description:module.DESCRIPTION, icon:module.ICON, entriesNum: module.ENTRIES_NUMBER, url: module.URL});
                });
            }).finally(function() {
               
            });
            return dashboardApps;
        };
                
        return dashboardManage;
    }]);
})(angular);
