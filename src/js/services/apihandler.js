//window.sessionId = "E823A46404E87EF1BEF4005056B7F725";
//window.sessionId =false;
window.language= "IT";

(function(angular) {
    'use strict';
    angular.module('ddmApps').service('apiHandler', ['$http', '$q', '$window', '$translate', '$cookies', '$httpParamSerializer', 'Upload', 
        function ($http, $q, $window, $translate, $cookies, $httpParamSerializer, Upload) {

        $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        var sessionId = '';
        var ApiHandler = function() {
            this.inprocess = false;
            this.asyncSuccess = false;
            this.error = '';
            //this.usermanage = new UserManage();
            //this.sessionId = "E823A46404E87EF1BEF4005056B7F725"; //$cookies.get("DDMSESSIONID");
            //this.test = fileManagerCtrl.ddd;
            
        };
        ApiHandler.prototype.deferredHandler = function(data, deferred, code, defaultMsg) {
            
            if (!data || typeof data !== 'object') {
                this.error = 'Error %s - Bridge response error, please check the API docs or this ajax response.'.replace('%s', code);
            }
            if (code == 404) {
                this.error = 'Error 404 - Backend bridge is not working, please check the ajax response.';
            }
            if (data.RETURN_CODE !== '000' ) {
                console.log(data);
                //this.error = "SAP Error";
                this.error = data.MESSAGES[0].MESSAGE;
            }
            
           /* if (data.RETURN_CODE === '000'){
                
            }*/
            
            /* if (!this.error && data.error) {
                this.error = data.MESSAGES[0].MESSAGE;
            } 
            if (!this.error && defaultMsg) {
                this.error = defaultMsg;
            } */
            if (this.error) {
                console.log(this.error);
                return deferred.reject(false);
            }
            return deferred.resolve(data);
        };
        
        ApiHandler.prototype.setSessionId = function(paramSessionId){
            sessionId = paramSessionId;
        };
        
        //Retrieve SAP document-list
        ApiHandler.prototype.list = function(apiUrl, path, dockey, listType, customDeferredHandler, exts) {
            var self = this;
            var dfHandler = customDeferredHandler || self.deferredHandler;
            var deferred = $q.defer();
            var data = {               
                action: 'list', 
                path: path, 
                fileExtensions: exts && exts.length ? exts : undefined,
                
                //SAP
                oper: 'get_list',
                sessionId: sessionId,
                langu: language,
                dockey: dockey,
                listtype: listType
            };

            self.inprocess = true;
            self.error = '';

            $http.post(apiUrl, data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(function(response) {
               // console.log(response.data.jsondata.ITEMS);
                dfHandler(response.data.jsondata, deferred, response.status);
            }, function(response) {
                dfHandler(response.data.jsondata, deferred, response.status, 'Unknown error listing, check the response');
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.copy = function(apiUrl, items, path, singleFilename) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'copy',
                items: items, //dockeyes
                newPath: path //folderDocKey
            };

            if (singleFilename && items.length === 1) {
                data.singleFilename = singleFilename;
            }

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data).then(function(response) {
                self.deferredHandler(response.data, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_copying'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.move = function(apiUrl, items, path) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'move',
                items: items,
                newPath: path
                //dockeys
                //folderDocKey cartella di partenza
                //newFolderDocKey cartella di destinazione
                
            };
            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data).then(function(response) {
                self.deferredHandler(response.data, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_moving'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.remove = function(apiUrl, items) {
            
            console.log("RemoveItems:");
            console.log(items);
            
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'remove',
                //dockeys
                
                //SAP
                oper: 'delete',
                sessionId: sessionId,
                langu: language,  
                items: items
            };

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status, $translate.instant('error_deleting'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };
        
        
        ApiHandler.prototype.manageFavourites = function(apiUrl, items, addFavourite) {
            var flag = 'X';
            if (addFavourite === false){
                flag = '';
            }
            
            var self = this;
            var deferred = $q.defer();
            var data = {
                oper: 'MANAGE_FAVOURITE',
                //dockeys
                
                sessionId: sessionId,
                langu: language,  
                items: items,
                flag: flag
            };

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status, $translate.instant('error_deleting'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.upload = function(apiUrl, destination, dockey, files) {
            var self = this;
            var deferred = $q.defer();
            self.inprocess = true;
            self.progress = 0;
            self.error = '';

            var data = {
                oper: "UPLOAD",
                destination: destination,
                folderDocKey: dockey,
                sessionId: sessionId,
                langu: language
                //fileName
            };

            for (var i = 0; i < files.length; i++) {
                data['file-' + i] = files[i];
            }

            if (files && files.length) {
                Upload.upload({
                    url: apiUrl,
                    data: data
                }).then(function (data) {
                    self.deferredHandler(data.data.jsondata, deferred, data.status);
                }, function (data) {
                    self.deferredHandler(data.data.jsondata, deferred, data.status, 'Unknown error uploading files');
                }, function (evt) {
                    self.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total)) - 1;
                })['finally'](function() {
                    self.inprocess = false;
                    self.progress = 0;
                });
            }

            return deferred.promise;
        };

        ApiHandler.prototype.getContent = function(apiUrl, itemPath) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'getContent',
                item: itemPath
            };

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data).then(function(response) {
                console.log(response);
                self.deferredHandler(response.data, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_getting_content'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };
        
        //Custom: getDetails
        ApiHandler.prototype.getDetails = function(apiUrl, itemPath) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'getDetails',
                item: itemPath //dockey
            };

            $http.post(apiUrl, data).then(function(response) {
                console.log(response);
                self.deferredHandler(response.data, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_gettingPath'));
            })['finally'](function() {
                self.inprocess = false;
            });
            
            return deferred.promise;
        };

        ApiHandler.prototype.edit = function(apiUrl, itemPath, content) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'edit',
                item: itemPath,
                content: content
            };

            self.inprocess = true;
            self.error = '';

            $http.post(apiUrl, data).then(function(response) {
                console.log(response);
                self.deferredHandler(response.data, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_modifying'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.rename = function(apiUrl, itemPath, newPath, dockey, newName) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'rename',
                item: itemPath,
                newItemPath: newPath,
                
                //SAP
                oper: 'rename',
                sessionId: sessionId,
                langu: language,
                dockey: dockey,
                newname: newName
            };
            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status, $translate.instant('error_renaming'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.getUrl = function(apiUrl, path) {
            var data = {
                action: 'download',
                path: path
                //docKey
            };
            return path && [apiUrl, $httpParamSerializer(data)].join('?');
        };
        

        
        //Custom: getCompletePath
        ApiHandler.prototype.getCompletePath = function(apiUrl, path) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'getPath',
                item: path 
            };
            
            $http.post(apiUrl, data).then(function(response) {
                self.deferredHandler(response.data, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_gettingPath'));
            })['finally'](function() {
                self.inprocess = false;
            });
        };
        
        //SAP download/viewfile
        ApiHandler.prototype.download = function(apiUrl, itemPath, toFilename, dockey, downloadByAjax, forceNewWindow) {
            var self = this;
            //var url = this.getUrl(apiUrl, itemPath);

            /*if (!downloadByAjax || forceNewWindow || !$window.saveAs) {
                !$window.saveAs && $window.console.log('Your browser dont support ajax download, downloading by default');
                return !!$window.open(url, '_blank', '');
            }*/

            var deferred = $q.defer();
           /* $http.get(url).then(function(response) {
                var bin = new $window.Blob([response.data]);
                deferred.resolve(response.data);
                $window.saveAs(bin, toFilename);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_downloading'));
            })['finally'](function() {
                self.inprocess = false;
            });*/
            
            var data = {
                action: 'download',
                //item: itemPath,
                
                //SAP
                oper: 'download',
                sessionId: sessionId,
                langu: language,
                dockey: dockey
            };
            
            $http.post(apiUrl, data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status, $translate.instant('error_renaming'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;        
        };

        ApiHandler.prototype.downloadMultiple = function(apiUrl, items, toFilename, downloadByAjax, forceNewWindow) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'downloadMultiple',
                items: items,
                toFilename: toFilename
            };
            var url = [apiUrl, $httpParamSerializer(data)].join('?');

            if (!downloadByAjax || forceNewWindow || !$window.saveAs) {
                !$window.saveAs && $window.console.log('Your browser dont support ajax download, downloading by default');
                return !!$window.open(url, '_blank', '');
            }

            self.inprocess = true;
            $http.get(apiUrl).then(function(response) {
                var bin = new $window.Blob([response.data]);
                deferred.resolve(response.data);
                $window.saveAs(bin, toFilename);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_downloading'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.compress = function(apiUrl, items, compressedFilename, path) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'compress',
                items: items,
                destination: path,
                compressedFilename: compressedFilename
            };

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data).then(function(response) {
                self.deferredHandler(response.data, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_compressing'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.extract = function(apiUrl, item, folderName, path) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'extract',
                item: item,
                destination: path,
                folderName: folderName
            };

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data).then(function(response) {
                self.deferredHandler(response.data, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_extracting'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.changePermissions = function(apiUrl, items, permsOctal, permsCode, recursive) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'changePermissions',
                items: items,
                perms: permsOctal,
                permsCode: permsCode,
                recursive: !!recursive
            };

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data).then(function(response) {
                self.deferredHandler(response.data, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data, deferred, response.status, $translate.instant('error_changing_perms'));
            })['finally'](function() {
                self.inprocess = false;
            });
            return deferred.promise;
        };

        ApiHandler.prototype.createFolder = function(apiUrl, path, dockey, foldername) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'createfolder', 
                newPath: path,
       
                //SAP
                oper: 'create_folder',
                sessionId: sessionId,
                langu: language,
                foldername: foldername,
                folderdockey: dockey   
                
            };

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status, $translate.instant('error_creating_folder'));
            })['finally'](function() {
                self.inprocess = false;
            });

            return deferred.promise;
        };
        
        
        /***dashboardManage***/
        ApiHandler.prototype.getDashboard = function(apiUrl) {
            var self = this;
            var deferred = $q.defer();
            var data = {
                action: 'get_dashboard',        
                //SAP
                oper: 'get_dashboard',
                sessionId: sessionId,
                langu: language   
            };

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status);
            }, function(response) {
                self.deferredHandler(response.data.jsondata, deferred, response.status, $translate.instant('error_creating_folder'));
            })['finally'](function() {
                self.inprocess = false;
            });

            return deferred.promise;
            
        };
        
        
        /***appLogin***/
        ApiHandler.prototype.appLogin = function(apiUrl, user, pw, lang) {
            var self = this;
            var deferred = $q.defer();
            /*var data = {
                action: 'LOGIN',        
                //SAP
                oper: 'LOGIN',
                user: user,
                password: pw,
                lang: lang
            };*/
            
            var data = 'oper=LOGIN&' + 'username=' + user + '&password=' + pw + '&lang=' + lang;

            self.inprocess = true;
            self.error = '';
            $http.post(apiUrl, data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(
            //successo
            function(response) {
                console.log(response);
                self.deferredHandler(response.data.jsondata, deferred, response.status);
            }, 
            //errore
            function(response) {
                console.log('Errore promise');
                self.deferredHandler(response.data.jsondata, deferred, response.status, 'errore');
            })['finally'](function() {
                self.inprocess = false;
            });

            return deferred.promise;
            
        };

        return ApiHandler;

    }]);
})(angular);
