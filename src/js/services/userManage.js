(function(angular) {
    'use strict';
    angular.module('ddmApps').service('userManage', [
        'apiMiddleware', 'fileManagerConfig', '$cookies', function (ApiMiddleware, fileManagerConfig, $cookies) {

        var UserManage = function() {
            this.apiMiddleware = new ApiMiddleware();
            //userData structure
            this.userData = {   sessionId : this.getSessionId(),
                                nameFirst : '',
                                nameLast : '',
                                email : '',
                                username : ''
                            };
        };
               
        UserManage.prototype.deferredHandler = function(data, deferred, code, defaultMsg) {
            if (!data || typeof data !== 'object') {
                this.error = 'Error %s - Bridge response error, please check the API docs or this ajax response.'.replace('%s', code);
            }
            if (code == 404) {
                this.error = 'Error 404 - Backend bridge is not working, please check the ajax response.';
            }
            if (code == 200) {
                this.error = null;
            }
            if (!this.error && data.RETURN_CODE !== '000') {
                this.error = data.MESSAGES.MESSAGE;
            }
            /*if (!this.error && data.error) {
                this.error = data.error.message;
            }
            if (!this.error && defaultMsg) {
                this.error = defaultMsg;
            }*/
            if (this.error) {
                return deferred.reject(data);
            }
            return deferred.resolve(data);
        };

        /*UserManage.prototype.login = function() {
            var self = this;

            //var returnValue = false;
            this.apiMiddleware.appLogin(this.loginData.user, this.loginData.pw, this.loginData.lang = 'IT').then(function(data){
                self.setUserData(data);
                self.storeSessionId(data);
                
            },
            function (error){
                //Error
            });

        };*/
        
       UserManage.prototype.setUserData = function(data){
            this.userData.sessionId = data.SESSIONID;
            this.userData.nameFirst = data.USER_INFO.NAME_FIRST;
            this.userData.nameLast = data.USER_INFO.NAME_LAST;
            this.userData.email = data.USER_INFO.SMTP_ADDR;
            this.userData.username = data.USER_INFO.USERNAME;
        };  
        
        UserManage.prototype.storeSessionId = function(data){
            $cookies.put("DDMSESSIONID", "E823A46404E87EF1BEF4005056B7F725");
            return true;
            //$cookies.put("DDMSESSIONID", data.SESSIONID);    
        };
        
        UserManage.prototype.isUserLoggedIn = function (){
            var sessionId = this.getSessionId();
            console.log("sessionid", sessionId);
            if (sessionId!==undefined){
                return true;
            } else {
                return false;
            }
        };
        
        UserManage.prototype.getSessionId = function (){
            //return "E823A46404E87EF1BEF4005056B7F725";
            return $cookies.get("DDMSESSIONID");
        };
        
        
        return UserManage;
    }]);
})(angular);
